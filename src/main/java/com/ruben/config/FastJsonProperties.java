package com.ruben.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import cn.hutool.core.date.DatePattern;
import lombok.Data;

/**
 * 配置文件
 *
 * @author <achao1441470436@gmail.com>
 * @since 2022/3/20 18:10
 */
@Data
@ConfigurationProperties(FastJsonProperties.PREFIX)
public class FastJsonProperties {

    public static final String PREFIX = "fastjson";

    private String datePattern = DatePattern.NORM_DATETIME_PATTERN;
    private String localDateTimePattern = DatePattern.NORM_DATETIME_PATTERN;
    private String localDatePattern = DatePattern.NORM_DATE_PATTERN;

}
