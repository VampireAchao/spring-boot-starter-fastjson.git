package com.ruben.config;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

/**
 * @author <achao1441470436@gmail.com>
 * @since 2022/3/20 17:53
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Inherited
// 重点在这注解导入启动配置
@Import({FastJsonAutoConfiguration.class})
public @interface EnableFastJson {
}
